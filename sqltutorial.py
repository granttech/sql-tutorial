import sqlite3
from sqlite3 import Error


def main():
    try:
        sqliteConnection = sqlite3.connect('chinook.db')
        cursor = sqliteConnection.cursor()
        print("Connected to SQLite")

        sqlite_select_query = """SELECT Albums.AlbumId, Albums.ArtistId, Albums.Title, Artists.Name 
            from Albums 
            join Artists on Albums.ArtistId = Artists.ArtistId
            limit 10"""
        cursor.execute(sqlite_select_query)
        records = cursor.fetchall()
        print("Total rows are:  ", len(records))
        print("Printing each row")
        for row in records:
            # print(row)
            print("AlbumId: ", row[0])
            print("ArtistId: ", row[1])
            print("Title: ", row[2])
            print("Name: ", row[3])
            print("\n")

        cursor.close()

    except sqlite3.Error as error:
        print("Failed to read data from sqlite table", error)
    finally:
        if (sqliteConnection):
            sqliteConnection.close()
            print("The SQLite connection is closed")


if __name__ == '__main__':
    main()
