import sqlite3
from sqlite3 import Error


class School:
    def __init__(self):
        self.schoolId = None
        self.name = None

    def loadById(self, id):
        try:
            sqliteConnection = sqlite3.connect('students.db')
            cursor = sqliteConnection.cursor()

            cursor.execute("SELECT schoolId, name FROM schools WHERE schoolId=?", (id,))

            records = cursor.fetchall()
            if len(records) >= 1:
                row = records[0]
                self.schoolId = row[0]
                self.name = row[1]
            cursor.close()

        except sqlite3.Error as error:
            print("Failed to read data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def print(self):
        print("schoolId: ", self.schoolId)
        print("name: ", self.name)

    def listAll(self):
        try:
            sqliteConnection = sqlite3.connect('students.db')
            cursor = sqliteConnection.cursor()

            cursor.execute("SELECT schoolId, name FROM schools")

            records = cursor.fetchall()
            if len(records) >= 1:
                for record in records:
                    print(str(record[0]) + " : " + str(record[1]))
            else:
                print("No schools")
            cursor.close()

        except sqlite3.Error as error:
            print("Failed to read data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()


class Student:

    def __init__(self):
        self.studentId = None
        self.name = None
        self.age = None
        self.height = None
        self.schoolId = None

    def migrate(self):
        # check if database exists
        f = False
        try:
            f = open("students.db")
            # already exists
        except IOError:
            print("Migrating database")
            sqliteConnection = sqlite3.connect('students.db')
            sqliteConnection.execute("""CREATE TABLE IF NOT EXISTS schools(
                        schoolId integer PRIMARY KEY,
                        name varchar(250) NOT NULL
                        )""")
            sqliteConnection.commit()
            sqliteConnection.execute("""CREATE TABLE IF NOT EXISTS students(
                        studentId integer PRIMARY KEY,
                        name varchar(250) NOT NULL,
                        age integer NOT NULL,
                        height integer NOT NULL,
                        schoolId integer NOT NULL,
                        FOREIGN KEY (schoolId) REFERENCES schools(schoolId)
                        )""")
            sqliteConnection.commit()
            cursor = sqliteConnection.cursor()
            cursor.execute("INSERT INTO schools(name) VALUES('Wellingborough School')")
            sqliteConnection.commit()
            cursor.execute("INSERT INTO schools(name) VALUES('Wrenn School')")
            sqliteConnection.commit()
            cursor.execute("INSERT INTO schools(name) VALUES('Northampton School for Boys')")
            sqliteConnection.commit()
            cursor.execute("INSERT INTO schools(name) VALUES('Northampton School for Girls')")
            sqliteConnection.commit()

        finally:
            if f:
                f.close()

    def loadById(self, id):
        try:
            sqliteConnection = sqlite3.connect('students.db')
            cursor = sqliteConnection.cursor()

            # sqlite_select_query = "SELECT * FROM students WHERE studentId={id}".format(id=id)
            # cursor.execute(sqlite_select_query)

            cursor.execute("SELECT studentId,name,age,height,schoolId FROM students WHERE studentId=?", (id,))

            records = cursor.fetchall()
            if len(records) >= 1:
                row = records[0]
                self.studentId = row[0]
                self.name = row[1]
                self.age = row[2]
                self.height = row[3]
                self.schoolId = row[4]
            cursor.close()

        except sqlite3.Error as error:
            print("loadById : Failed to read data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def refresh(self):
        self.loadById(self.schoolId)

    def find(self):
        search = input("Who are you searching for? : ")
        try:
            sqliteConnection = sqlite3.connect('students.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("""SELECT st.studentId, st.name, st.age, st.height, st.schoolId, sc.name as schoolName
                FROM students st
                LEFT OUTER JOIN schools sc on sc.schoolId = st.schoolId
                WHERE st.name LIKE(?)""", ('%' + search + '%',))

            records = cursor.fetchall()
            if len(records) >= 1:
                for row in records:
                    print("studentId: ", row[0])
                    print("name: ", row[1])
                    print("age: ", row[2])
                    print("height: ", row[3])
                    print("school: ", row[5])
                cursor.close()
                sqliteConnection.close()
                selectedStudent = input("Which student do you want to select? Enter ID : ")
                self.loadById(selectedStudent)
                return True

            else:
                print("No matching students found\n");
                cursor.close()
                sqliteConnection.close()
                return False

        except sqlite3.Error as error:
            print("find : Failed to read data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def list(self):
        try:
            sqliteConnection = sqlite3.connect('students.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("""SELECT st.studentId, st.name, st.age, st.height, st.schoolId, sc.name as schoolName
                FROM students st
                LEFT OUTER JOIN schools sc on sc.schoolId = st.schoolId""")

            records = cursor.fetchall()
            if len(records) >= 1:
                for row in records:
                    print("studentId: ", row[0])
                    print("name: ", row[1])
                    print("age: ", row[2])
                    print("height: ", row[3])
                    print("school: ", row[5])
                cursor.close()
                sqliteConnection.close()

            else:
                cursor.close()
                sqliteConnection.close()
                return False

        except sqlite3.Error as error:
            print("find : Failed to read data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def delete(self):
        try:
            sqliteConnection = sqlite3.connect('students.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("DELETE FROM students WHERE studentId=?",
                           (self.studentId,))
            sqliteConnection.commit()

        except sqlite3.Error as error:
            print("delete : Failed to delete data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def add(self, newName, newAge, newHeight, newSchoolId):
        newStudentId = False
        try:
            sqliteConnection = sqlite3.connect('students.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("INSERT INTO students(name, age, height, schoolId) VALUES(?, ?, ?, ?)",
                           (newName, newAge, newHeight, newSchoolId))
            newStudentId = cursor.lastrowid
            sqliteConnection.commit()

        except sqlite3.Error as error:
            print("add : Failed to delete data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()
            if newStudentId:
                return newStudentId
            else:
                return False

    def print(self):
        print("studentId: ", self.studentId)
        print("name: ", self.name)
        print("age: ", self.age)
        print("height: ", self.height)
        print("schoolId: ", self.schoolId)

    def save(self):
        try:
            sqliteConnection = sqlite3.connect('students.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("UPDATE students SET name=?, age=?, height=?, schoolId=? WHERE studentId=?",
                           (self.name, self.age, self.height, self.schoolId, self.studentId))
            sqliteConnection.commit()

        except sqlite3.Error as error:
            print("save : Failed to update data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()


def main():
    students = Student()
    students.migrate()
    while True:
        print("What would you like to do? ")
        print("(A)dd, (E)dit (L)ist", )
        command = input("> ")
        if command in ('e', 'E'):
            student = Student()
            studentFound = student.find()
            if studentFound:
                print("You are now working with - " + student.name)
                print("What would you like to do? ")
                print("(U)pdate, (D)elete")
                command = input("> ")
                if command in ('u', 'U'):
                    # update
                    newName = input('New name : ')
                    student.name = newName
                    newAge = input("New age : ")
                    student.age = int(newAge)
                    newHeight = input("New height : ")
                    student.height = int(newHeight)
                    school = School()
                    school.listAll()
                    newSchoolId = input("New school id : ")
                    student.schoolId = int(newSchoolId)
                    student.save()
                    student.refresh()
                    student.print()
                elif command in ('d', 'D'):
                    # delete
                    student.delete()
                    print("student deleted")
            else:
                print("No student found")
        elif command in ('a', 'A'):
            newName = input("student name : ")
            newAge = input("age : ")
            newAge = int(newAge)
            newHeight = input("height : ")
            newHeight = int(newHeight)
            school = School()
            school.listAll()
            newSchoolId = input("schoold id : ")
            student = Student()
            newstudentId = student.add(newName, newAge, newHeight, newSchoolId)
            if newstudentId:
                student.loadById(newstudentId)
                student.print()
            else:
                print("Failed to insert student")
        elif command in ('l', 'L'):
            student = Student()
            student.list()


if __name__ == '__main__':
    main()
