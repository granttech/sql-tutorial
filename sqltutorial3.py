import sqlite3
from sqlite3 import Error


class Album:

    def __init__(self):
        self.AlbumId = None
        self.ArtistId = None
        self.Title = None

    def loadById(self, id):
        try:
            sqliteConnection = sqlite3.connect('chinook.db')
            cursor = sqliteConnection.cursor()

            # sqlite_select_query = "SELECT * FROM albums WHERE AlbumId={id}".format(id=id)
            # cursor.execute(sqlite_select_query)

            cursor.execute("SELECT * FROM albums WHERE AlbumId=?", (id,))

            records = cursor.fetchall()
            if len(records) >= 1:
                row = records[0]
                self.AlbumId = row[0]
                self.Title = row[1]
                self.ArtistId = row[2]
            cursor.close()

        except sqlite3.Error as error:
            print("Failed to read data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def refresh(self):
        self.loadById(self.AlbumId)

    def find(self):
        search = input("What are you searching for? : ")
        try:
            sqliteConnection = sqlite3.connect('chinook.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("SELECT * FROM albums WHERE Title LIKE(?)", ('%' + search + '%',))

            records = cursor.fetchall()
            if len(records) >= 1:
                for row in records:
                    print("AlbumId: ", row[0])
                    print("Title: ", row[1])
                    print("ArtistId: ", row[2])
                cursor.close()
                sqliteConnection.close()
                selectedAlbum = input("Which album do you want to select? Enter ID : ")
                self.loadById(selectedAlbum)
                return True

            else:
                print("No matching albums found\n");
                cursor.close()
                sqliteConnection.close()
                return False


        except sqlite3.Error as error:
            print("Failed to read data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def delete(self):
        try:
            sqliteConnection = sqlite3.connect('chinook.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("DELETE FROM albums WHERE AlbumId=?",
                           (self.AlbumId,))
            sqliteConnection.commit()

        except sqlite3.Error as error:
            print("Failed to delete data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def add(self, newTitle, newArtistId):
        try:
            sqliteConnection = sqlite3.connect('chinook.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("INSERT INTO albums(Title, ArtistId) VALUES(?, ?)",
                           (newTitle, newArtistId))
            newAlbumId = cursor.lastrowid
            sqliteConnection.commit()

        except sqlite3.Error as error:
            print("Failed to delete data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()
            if newAlbumId:
                return newAlbumId
            else:
                return False

    def print(self):
        print("AlbumId: ", self.AlbumId)
        print("ArtistId: ", self.ArtistId)
        print("Title: ", self.Title)

    def save(self):
        try:
            sqliteConnection = sqlite3.connect('chinook.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("UPDATE albums SET ArtistId=?, Title=? WHERE AlbumId=?",
                           (self.ArtistId, self.Title, self.AlbumId))
            sqliteConnection.commit()

        except sqlite3.Error as error:
            print("Failed to update data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()


def main():
    while True:
        print("What would you like to do? ")
        print("(A)dd, (E)dit")
        command = input("> ")
        if command in ('e', 'E'):
            album = Album()
            albumFound = album.find()
            if albumFound:
                print("You are now working with - " + album.Title)
                print("What would you like to do? ")
                print("(U)pdate, (D)elete")
                command = input("> ")
                if command in ('u', 'U'):
                    # update
                    newTitle = input('New title : ')
                    album.Title = newTitle
                    album.save()
                    album.refresh()
                    album.print()
                elif command in ('d', 'D'):
                    # delete
                    album.delete()
                    print("Album deleted")
            else:
                print("No album found")
        elif command in ('a', 'A'):
            newTitle = input("Album title : ")
            newArtistId = input("Artist Id : ")
            album = Album()
            newAlbumId = album.add(newTitle, newArtistId)
            if newAlbumId:
                album.loadById(newAlbumId)
                album.print()
            else:
                print("Failed to insert album")

if __name__ == '__main__':
    main()
