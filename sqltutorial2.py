import sqlite3
from sqlite3 import Error


class Album:

    def __init__(self):
        self.AlbumId = None
        self.ArtistId = None
        self.Title = None

    def loadById(self, id):
        try:
            sqliteConnection = sqlite3.connect('chinook.db')
            cursor = sqliteConnection.cursor()

            # sqlite_select_query = "SELECT * FROM albums WHERE AlbumId={id}".format(id=id)
            # cursor.execute(sqlite_select_query)

            cursor.execute("SELECT * FROM albums WHERE AlbumId=?", (id,))

            records = cursor.fetchall()
            if len(records) >= 1:
                row = records[0]
                self.AlbumId = row[0]
                self.Title = row[1]
                self.ArtistId = row[2]
            cursor.close()

        except sqlite3.Error as error:
            print("Failed to read data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()

    def print(self):
        print("AlbumId: ", self.AlbumId)
        print("ArtistId: ", self.ArtistId)
        print("Title: ", self.Title)
        print("\n")

    def save(self):
        try:
            sqliteConnection = sqlite3.connect('chinook.db')
            cursor = sqliteConnection.cursor()
            cursor.execute("UPDATE albums SET ArtistId=?, Title=? WHERE AlbumId=?",
                           (self.ArtistId, self.Title, self.AlbumId))
            sqliteConnection.commit()

        except sqlite3.Error as error:
            print("Failed to update data from sqlite table", error)
        finally:
            if (sqliteConnection):
                sqliteConnection.close()


def main():
    album = Album()
    album.loadById(9)
    album.print()
    album.Title = "Plays Metallica By Three Cellos"
    album.save()
    album.loadById(9)
    album.print()


if __name__ == '__main__':
    main()
